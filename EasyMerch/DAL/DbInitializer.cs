﻿using EasyMerch.Models.Dictionaries;
using EasyMerch.Models.General;
using EasyMerch.Models.ScaleSizes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EasyMerch.DAL
{
    public class DbInitializer : DropCreateDatabaseAlways<MerchContext>
    {

        protected override void Seed(MerchContext conext)
        {

            Area area = new Area { Id = Guid.NewGuid(), Name = "MAGAZIN" };
            Brand brand = new Brand { Id = Guid.NewGuid(), Name = "ARMYANI"};
            CostSegment costSegment = new CostSegment { Id = Guid.NewGuid(), Name = "Luxury" };
            Family family = new Family { Id = Guid.NewGuid(), Name = "WOMEN" };
            KTT ktt = new KTT { Id = Guid.NewGuid(), Name = "T-SHIRT", ParentId = Guid.Empty };
            Currency currency = new Currency { Id = Guid.NewGuid(), Name = "EURO" };
            OrderType orderType = new OrderType { Id = Guid.NewGuid(), Name = "General" };
            Buyer buyer = new Buyer { Id = Guid.NewGuid(), FirstName = "Alexander", LastName = "Shatilov" };
            Collection collection = new Collection { Id = Guid.NewGuid(), Name = "Super fashion" };
            Provider provider = new Provider { Id = Guid.NewGuid(), Name = "CHINA FACTORY" };
            Season season = new Season
            {
                Id = Guid.NewGuid(),
                Name = "SS2016",
                StartDate = new DateTime(2016, 01, 01),
                EndDate = new DateTime(2016, 01, 30)
            };


            Good good = new Good
            {
                Id = Guid.NewGuid(),
                Article = "AAA02B",
                BestSeller = true,
                CarryOver = true,
                ColorCode = "RED234",
                CostSegment = costSegment,
                Family = family,
                KTT = ktt,
                MaterialCode = "COTTON54",
                Permament = true
            };

            Order order = new Order
            {
                Id = Guid.NewGuid(),
                Area = area,
                Brand = brand,
                Currency = currency,
                OrderType = orderType,
                Buyer = buyer,
                Collection = collection,
                Provider = provider,
                Season = season

            };

            Scale scale = new Scale
            {
                Id = Guid.NewGuid(),
                Name = "europ"
            };

            Size size = new Size
            {
                Id = Guid.NewGuid(),
                Name = "XXL",
                Scale = scale
            };

            

            OrderGood orderGood = new OrderGood
            {
                Id = Guid.NewGuid(),
                GoodId = good.Id,
                OrderId = order.Id
            };

            OrderGoodDetails ordGoodDet = new OrderGoodDetails
            {
                Id = Guid.NewGuid(),
                Markup = 55,
                PurchasePrice = 456,
                OrderGood = orderGood
                
            };

            SizeValue sizeValue = new SizeValue
            {
                Id = Guid.NewGuid(),
                Size = size,
                OrderAmmount = 50,
                OrderConfirmationAmmoount = 30,
                OrderDeliveryAmmount = 32,
                OrderGoodDetails = ordGoodDet
            };



            conext.CostSegments.Add(costSegment);
            conext.Families.Add(family);
            conext.KTTs.Add(ktt);
            conext.Currencies.Add(currency);
            conext.Brands.Add(brand);
            conext.Buyers.Add(buyer);
            conext.Seasons.Add(season);
            conext.Providers.Add(provider);
            conext.Collections.Add(collection);
            conext.Areas.Add(area);

            conext.Goods.Add(good);
            conext.Orders.Add(order);
            conext.Scales.Add(scale);
            conext.Sizes.Add(size);
            conext.OrderGoods.Add(orderGood);
            conext.OrderGoodDetails.Add(ordGoodDet);
            conext.SizeValues.Add(sizeValue);

            
            base.Seed(conext);
        }
    }
}