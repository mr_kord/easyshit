﻿using EasyMerch.Models.Dictionaries;
using EasyMerch.Models.General;
using EasyMerch.Models.ScaleSizes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace EasyMerch.DAL
{
    public class MerchContext : DbContext
    {
        public DbSet<Area> Areas { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Collection> Collections { get; set; }
        public DbSet<CostSegment> CostSegments { get; set; }
        public DbSet<Currency> Currencies { get; set; }
        public DbSet<OrderType> OrderTypes { get; set; }
        public DbSet<Provider> Providers { get; set; }
        public DbSet<Season> Seasons { get; set; }
        public DbSet<Buyer> Buyers { get; set; }
        public DbSet<Family> Families { get; set; }
        public DbSet<KTT> KTTs { get; set; }

        public DbSet<Scale> Scales { get; set; }
        public DbSet<Size> Sizes { get; set; }
        public DbSet<SizeValue> SizeValues { get; set; }

        public DbSet<Good> Goods { get; set; }
        public DbSet<OrderGood> OrderGoods { get; set; }
        public DbSet<OrderGoodDetails> OrderGoodDetails { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Photo> Photos { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<OrderGood>().HasKey(k => new { k.OrderId, k.GoodId });

            modelBuilder.Entity<Order>()
                .HasMany(o => o.OrderGoods)
                .WithRequired()
                .HasForeignKey(k => k.OrderId);


            modelBuilder.Entity<Good>()
                .HasMany(g => g.OrderGoods)
                .WithRequired()
                .HasForeignKey(k => k.GoodId);
        }
    }
}