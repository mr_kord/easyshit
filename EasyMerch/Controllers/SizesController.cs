﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyMerch.DAL;
using EasyMerch.Models.Dictionaries;
using EasyMerch.DAL.Repositories;
using EasyMerch.Models.ScaleSizes;

namespace EasyMerch.Controllers
{
    public class SizesController : ApiController
    {
        private MerchContext db = new MerchContext();
        private IRepository<Size> sizeRepository;

        public SizesController()
        {
            this.sizeRepository = new Repository<Size>(db);
        }
        public SizesController(IRepository<Size> repository)
        {
            this.sizeRepository = repository;
        }


        // GET: api/Sizes
        public IQueryable<Size> GetSizes()
        {
            return sizeRepository.GetAll();
        }

        // GET: api/Sizes/5
        [ResponseType(typeof(Size))]
        public IHttpActionResult GetSize(Guid id)
        {
            Size Size = sizeRepository.GetById(id);
            if (Size == null)
            {
                return NotFound();
            }

            return Ok(Size);
        }

        // PUT: api/Sizes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSize(Size Size)
        {
            sizeRepository.Update(Size);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Sizes
        [ResponseType(typeof(Size))]
        public IHttpActionResult PostSize(Size Size)
        {
            Size.Id = Guid.NewGuid();
            sizeRepository.Insert(Size);

            return CreatedAtRoute("DefaultApi", new { id = Size.Id }, Size);
        }

        // DELETE: api/Sizes/5
        [ResponseType(typeof(Size))]
        public IHttpActionResult DeleteSize(Guid id)
        {
            Size Size = sizeRepository.GetById(id);
            if (Size == null)
            {
                return NotFound();
            }
            sizeRepository.Delete(Size);

            return Ok(Size);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}