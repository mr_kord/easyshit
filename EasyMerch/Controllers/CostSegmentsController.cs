﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyMerch.DAL;
using EasyMerch.Models.Dictionaries;
using EasyMerch.DAL.Repositories;

namespace EasyMerch.Controllers
{
    public class CostSegmentsController : ApiController
    {
        private MerchContext db = new MerchContext();
        private IRepository<CostSegment> costSegmentRepository;

        public CostSegmentsController()
        {
            this.costSegmentRepository = new Repository<CostSegment>(db);
        }
        public CostSegmentsController(IRepository<CostSegment> repository)
        {
            this.costSegmentRepository = repository;
        }


        // GET: api/CostSegments
        public IQueryable<CostSegment> GetCostSegments()
        {
            return costSegmentRepository.GetAll();
        }

        // GET: api/CostSegments/5
        [ResponseType(typeof(CostSegment))]
        public IHttpActionResult GetCostSegment(Guid id)
        {
            CostSegment CostSegment = costSegmentRepository.GetById(id);
            if (CostSegment == null)
            {
                return NotFound();
            }

            return Ok(CostSegment);
        }

        // PUT: api/CostSegments/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCostSegment(CostSegment CostSegment)
        {
            costSegmentRepository.Update(CostSegment);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/CostSegments
        [ResponseType(typeof(CostSegment))]
        public IHttpActionResult PostCostSegment(CostSegment CostSegment)
        {
            CostSegment.Id = Guid.NewGuid();
            costSegmentRepository.Insert(CostSegment);

            return CreatedAtRoute("DefaultApi", new { id = CostSegment.Id }, CostSegment);
        }

        // DELETE: api/CostSegments/5
        [ResponseType(typeof(CostSegment))]
        public IHttpActionResult DeleteCostSegment(Guid id)
        {
            CostSegment CostSegment = costSegmentRepository.GetById(id);
            if (CostSegment == null)
            {
                return NotFound();
            }
            costSegmentRepository.Delete(CostSegment);

            return Ok(CostSegment);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}