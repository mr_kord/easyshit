﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyMerch.DAL;
using EasyMerch.Models.Dictionaries;
using EasyMerch.DAL.Repositories;

namespace EasyMerch.Controllers
{
    public class KTTsController : ApiController
    {
        private MerchContext db = new MerchContext();
        private IRepository<KTT> kttRepository;

        public KTTsController()
        {
            this.kttRepository = new Repository<KTT>(db);
        }
        public KTTsController(IRepository<KTT> repository)
        {
            this.kttRepository = repository;
        }


        // GET: api/KTTs
        public IQueryable<KTT> GetKTTs()
        {
            return kttRepository.GetAll();
        }

        // GET: api/KTTs/5
        [ResponseType(typeof(KTT))]
        public IHttpActionResult GetKTT(Guid id)
        {
            KTT KTT = kttRepository.GetById(id);
            if (KTT == null)
            {
                return NotFound();
            }

            return Ok(KTT);
        }

        // PUT: api/KTTs/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutKTT(KTT KTT)
        {
            kttRepository.Update(KTT);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/KTTs
        [ResponseType(typeof(KTT))]
        public IHttpActionResult PostKTT(KTT KTT)
        {
            KTT.Id = Guid.NewGuid();
            kttRepository.Insert(KTT);

            return CreatedAtRoute("DefaultApi", new { id = KTT.Id }, KTT);
        }

        // DELETE: api/KTTs/5
        [ResponseType(typeof(KTT))]
        public IHttpActionResult DeleteKTT(Guid id)
        {
            KTT KTT = kttRepository.GetById(id);
            if (KTT == null)
            {
                return NotFound();
            }
            kttRepository.Delete(KTT);

            return Ok(KTT);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}