﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyMerch.DAL;
using EasyMerch.Models.Dictionaries;
using EasyMerch.DAL.Repositories;

namespace EasyMerch.Controllers
{
    public class OrderTypesController : ApiController
    {
        private MerchContext db = new MerchContext();
        private IRepository<OrderType> orderTypeRepository;

        public OrderTypesController()
        {
            this.orderTypeRepository = new Repository<OrderType>(db);
        }
        public OrderTypesController(IRepository<OrderType> repository)
        {
            this.orderTypeRepository = repository;
        }


        // GET: api/OrderTypes
        public IQueryable<OrderType> GetOrderTypes()
        {
            return orderTypeRepository.GetAll();
        }

        // GET: api/OrderTypes/5
        [ResponseType(typeof(OrderType))]
        public IHttpActionResult GetOrderType(Guid id)
        {
            OrderType OrderType = orderTypeRepository.GetById(id);
            if (OrderType == null)
            {
                return NotFound();
            }

            return Ok(OrderType);
        }

        // PUT: api/OrderTypes/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutOrderType(OrderType OrderType)
        {
            orderTypeRepository.Update(OrderType);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/OrderTypes
        [ResponseType(typeof(OrderType))]
        public IHttpActionResult PostOrderType(OrderType OrderType)
        {
            OrderType.Id = Guid.NewGuid();
            orderTypeRepository.Insert(OrderType);

            return CreatedAtRoute("DefaultApi", new { id = OrderType.Id }, OrderType);
        }

        // DELETE: api/OrderTypes/5
        [ResponseType(typeof(OrderType))]
        public IHttpActionResult DeleteOrderType(Guid id)
        {
            OrderType OrderType = orderTypeRepository.GetById(id);
            if (OrderType == null)
            {
                return NotFound();
            }
            orderTypeRepository.Delete(OrderType);

            return Ok(OrderType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}