﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyMerch.DAL;
using EasyMerch.Models.Dictionaries;
using EasyMerch.DAL.Repositories;

namespace EasyMerch.Controllers
{
    public class SeasonsController : ApiController
    {
        private MerchContext db = new MerchContext();
        private IRepository<Season> seasonRepository;

        public SeasonsController()
        {
            this.seasonRepository = new Repository<Season>(db);
        }
        public SeasonsController(IRepository<Season> repository)
        {
            this.seasonRepository = repository;
        }


        // GET: api/Seasons
        public IQueryable<Season> GetSeasons()
        {
            return seasonRepository.GetAll();
        }

        // GET: api/Seasons/5
        [ResponseType(typeof(Season))]
        public IHttpActionResult GetSeason(Guid id)
        {
            Season Season = seasonRepository.GetById(id);
            if (Season == null)
            {
                return NotFound();
            }

            return Ok(Season);
        }

        // PUT: api/Seasons/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSeason(Season Season)
        {
            seasonRepository.Update(Season);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Seasons
        [ResponseType(typeof(Season))]
        public IHttpActionResult PostSeason(Season Season)
        {
            Season.Id = Guid.NewGuid();
            seasonRepository.Insert(Season);

            return CreatedAtRoute("DefaultApi", new { id = Season.Id }, Season);
        }

        // DELETE: api/Seasons/5
        [ResponseType(typeof(Season))]
        public IHttpActionResult DeleteSeason(Guid id)
        {
            Season Season = seasonRepository.GetById(id);
            if (Season == null)
            {
                return NotFound();
            }
            seasonRepository.Delete(Season);

            return Ok(Season);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}