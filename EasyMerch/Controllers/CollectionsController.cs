﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyMerch.DAL;
using EasyMerch.Models.Dictionaries;
using EasyMerch.DAL.Repositories;

namespace EasyMerch.Controllers
{
    public class CollectionsController : ApiController
    {
        private MerchContext db = new MerchContext();
        private IRepository<Collection> collectionRepository;

        public CollectionsController()
        {
            this.collectionRepository = new Repository<Collection>(db);
        }
        public CollectionsController(IRepository<Collection> repository)
        {
            this.collectionRepository = repository;
        }


        // GET: api/Collections
        public IQueryable<Collection> GetCollections()
        {
            return collectionRepository.GetAll();
        }

        // GET: api/Collections/5
        [ResponseType(typeof(Collection))]
        public IHttpActionResult GetCollection(Guid id)
        {
            Collection Collection = collectionRepository.GetById(id);
            if (Collection == null)
            {
                return NotFound();
            }

            return Ok(Collection);
        }

        // PUT: api/Collections/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCollection(Collection Collection)
        {
            collectionRepository.Update(Collection);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Collections
        [ResponseType(typeof(Collection))]
        public IHttpActionResult PostCollection(Collection Collection)
        {
            Collection.Id = Guid.NewGuid();
            collectionRepository.Insert(Collection);

            return CreatedAtRoute("DefaultApi", new { id = Collection.Id }, Collection);
        }

        // DELETE: api/Collections/5
        [ResponseType(typeof(Collection))]
        public IHttpActionResult DeleteCollection(Guid id)
        {
            Collection Collection = collectionRepository.GetById(id);
            if (Collection == null)
            {
                return NotFound();
            }
            collectionRepository.Delete(Collection);

            return Ok(Collection);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}