﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyMerch.DAL;
using EasyMerch.Models.Dictionaries;
using EasyMerch.DAL.Repositories;
using EasyMerch.Models.General;

namespace EasyMerch.Controllers
{
    public class OrdersController : ApiController
    {
        private MerchContext db = new MerchContext();
        private IRepository<Order> orderRepository;

        public OrdersController()
        {
            this.orderRepository = new Repository<Order>(db);
        }
        public OrdersController(IRepository<Order> repository)
        {
            this.orderRepository = repository;
        }


        // GET: api/Orders
        public IQueryable<Order> GetOrders()
        {
            return orderRepository.GetAll();
        }

        // GET: api/Orders/5
        [ResponseType(typeof(Order))]
        public IHttpActionResult GetOrder(Guid id)
        {
            Order Order = orderRepository.GetById(id);
            if (Order == null)
            {
                return NotFound();
            }

            return Ok(Order);
        }

        // PUT: api/Orders/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutOrder(Order Order)
        {
            orderRepository.Update(Order);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Orders
        [ResponseType(typeof(Order))]
        public IHttpActionResult PostOrder(Order Order)
        {
            Order.Id = Guid.NewGuid();
            orderRepository.Insert(Order);

            return CreatedAtRoute("DefaultApi", new { id = Order.Id }, Order);
        }

        // DELETE: api/Orders/5
        [ResponseType(typeof(Order))]
        public IHttpActionResult DeleteOrder(Guid id)
        {
            Order Order = orderRepository.GetById(id);
            if (Order == null)
            {
                return NotFound();
            }
            orderRepository.Delete(Order);

            return Ok(Order);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}