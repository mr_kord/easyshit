﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyMerch.DAL;
using EasyMerch.Models.Dictionaries;
using EasyMerch.DAL.Repositories;

namespace EasyMerch.Controllers
{
    public class ProvidersController : ApiController
    {
        private MerchContext db = new MerchContext();
        private IRepository<Provider> providerRepository;

        public ProvidersController()
        {
            this.providerRepository = new Repository<Provider>(db);
        }
        public ProvidersController(IRepository<Provider> repository)
        {
            this.providerRepository = repository;
        }


        // GET: api/Providers
        public IQueryable<Provider> GetProviders()
        {
            return providerRepository.GetAll();
        }

        // GET: api/Providers/5
        [ResponseType(typeof(Provider))]
        public IHttpActionResult GetProvider(Guid id)
        {
            Provider Provider = providerRepository.GetById(id);
            if (Provider == null)
            {
                return NotFound();
            }

            return Ok(Provider);
        }

        // PUT: api/Providers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutProvider(Provider Provider)
        {
            providerRepository.Update(Provider);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Providers
        [ResponseType(typeof(Provider))]
        public IHttpActionResult PostProvider(Provider Provider)
        {
            Provider.Id = Guid.NewGuid();
            providerRepository.Insert(Provider);

            return CreatedAtRoute("DefaultApi", new { id = Provider.Id }, Provider);
        }

        // DELETE: api/Providers/5
        [ResponseType(typeof(Provider))]
        public IHttpActionResult DeleteProvider(Guid id)
        {
            Provider Provider = providerRepository.GetById(id);
            if (Provider == null)
            {
                return NotFound();
            }
            providerRepository.Delete(Provider);

            return Ok(Provider);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}