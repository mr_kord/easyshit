﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyMerch.DAL;
using EasyMerch.Models.Dictionaries;
using EasyMerch.DAL.Repositories;
using EasyMerch.Models.ScaleSizes;

namespace EasyMerch.Controllers
{
    public class ScalesController : ApiController
    {
        private MerchContext db = new MerchContext();
        private IRepository<Scale> scaleRepository;

        public ScalesController()
        {
            this.scaleRepository = new Repository<Scale>(db);
        }
        public ScalesController(IRepository<Scale> repository)
        {
            this.scaleRepository = repository;
        }


        // GET: api/Scales
        public IQueryable<Scale> GetScales()
        {
            return scaleRepository.GetAll();
        }

        // GET: api/Scales/5
        [ResponseType(typeof(Scale))]
        public IHttpActionResult GetScale(Guid id)
        {
            Scale Scale = scaleRepository.GetById(id);
            if (Scale == null)
            {
                return NotFound();
            }

            return Ok(Scale);
        }

        // PUT: api/Scales/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutScale(Scale Scale)
        {
            scaleRepository.Update(Scale);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Scales
        [ResponseType(typeof(Scale))]
        public IHttpActionResult PostScale(Scale Scale)
        {
            Scale.Id = Guid.NewGuid();
            scaleRepository.Insert(Scale);

            return CreatedAtRoute("DefaultApi", new { id = Scale.Id }, Scale);
        }

        // DELETE: api/Scales/5
        [ResponseType(typeof(Scale))]
        public IHttpActionResult DeleteScale(Guid id)
        {
            Scale Scale = scaleRepository.GetById(id);
            if (Scale == null)
            {
                return NotFound();
            }
            scaleRepository.Delete(Scale);

            return Ok(Scale);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}