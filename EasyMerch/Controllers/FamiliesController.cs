﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyMerch.DAL;
using EasyMerch.Models.Dictionaries;
using EasyMerch.DAL.Repositories;

namespace EasyMerch.Controllers
{
    public class FamiliesController : ApiController
    {
        private MerchContext db = new MerchContext();
        private IRepository<Family> familisesRepository;

        public FamiliesController()
        {
            this.familisesRepository = new Repository<Family>(db);
        }
        public FamiliesController(IRepository<Family> repository)
        {
            this.familisesRepository = repository;
        }


        // GET: api/Families
        public IQueryable<Family> GetFamilies()
        {
            return familisesRepository.GetAll();
        }

        // GET: api/Families/5
        [ResponseType(typeof(Family))]
        public IHttpActionResult GetFamily(Guid id)
        {
            Family Family = familisesRepository.GetById(id);
            if (Family == null)
            {
                return NotFound();
            }

            return Ok(Family);
        }

        // PUT: api/Families/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutFamily(Family Family)
        {
            familisesRepository.Update(Family);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Families
        [ResponseType(typeof(Family))]
        public IHttpActionResult PostFamily(Family Family)
        {
            Family.Id = Guid.NewGuid();
            familisesRepository.Insert(Family);

            return CreatedAtRoute("DefaultApi", new { id = Family.Id }, Family);
        }

        // DELETE: api/Families/5
        [ResponseType(typeof(Family))]
        public IHttpActionResult DeleteFamily(Guid id)
        {
            Family Family = familisesRepository.GetById(id);
            if (Family == null)
            {
                return NotFound();
            }
            familisesRepository.Delete(Family);

            return Ok(Family);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}