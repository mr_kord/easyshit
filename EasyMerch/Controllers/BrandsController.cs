﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyMerch.DAL;
using EasyMerch.Models.Dictionaries;
using EasyMerch.DAL.Repositories;

namespace EasyMerch.Controllers
{
    public class BrandsController : ApiController
    {
        private MerchContext db = new MerchContext();
        private IRepository<Brand> brandRepository;

        public BrandsController()
        {
            this.brandRepository = new Repository<Brand>(db);
        }
        public BrandsController(IRepository<Brand> repository)
        {
            this.brandRepository = repository;
        }


        // GET: api/Brands
        public IQueryable<Brand> GetBrands()
        {
            return brandRepository.GetAll();
        }

        // GET: api/Brands/5
        [ResponseType(typeof(Brand))]
        public IHttpActionResult GetBrands(Guid id)
        {
            Brand Brand = brandRepository.GetById(id);
            if (Brand == null)
            {
                return NotFound();
            }

            return Ok(Brand);
        }

        // PUT: api/Brands/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutBrand(Brand Brand)
        {
            brandRepository.Update(Brand);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Brands
        [ResponseType(typeof(Brand))]
        public IHttpActionResult PostBrand(Brand Brand)
        {
            Brand.Id = Guid.NewGuid();
            brandRepository.Insert(Brand);

            return CreatedAtRoute("DefaultApi", new { id = Brand.Id }, Brand);
        }

        // DELETE: api/Brands/5
        [ResponseType(typeof(Brand))]
        public IHttpActionResult DeleteBrand(Guid id)
        {
            Brand Brand = brandRepository.GetById(id);
            if (Brand == null)
            {
                return NotFound();
            }

            brandRepository.Delete(Brand);

            return Ok(Brand);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}