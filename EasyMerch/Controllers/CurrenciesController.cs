﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyMerch.DAL;
using EasyMerch.Models.Dictionaries;
using EasyMerch.DAL.Repositories;

namespace EasyMerch.Controllers
{
    public class CurrenciesController : ApiController
    {
        private MerchContext db = new MerchContext();
        private IRepository<Currency> currencyRepository;

        public CurrenciesController()
        {
            this.currencyRepository = new Repository<Currency>(db);
        }
        public CurrenciesController(IRepository<Currency> repository)
        {
            this.currencyRepository = repository;
        }


        // GET: api/Currencies
        public IQueryable<Currency> GetCurrencies()
        {
            return currencyRepository.GetAll();
        }

        // GET: api/Currencies/5
        [ResponseType(typeof(Currency))]
        public IHttpActionResult GetCurrency(Guid id)
        {
            Currency Currency = currencyRepository.GetById(id);
            if (Currency == null)
            {
                return NotFound();
            }

            return Ok(Currency);
        }

        // PUT: api/Currencies/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutCurrency(Currency Currency)
        {
            currencyRepository.Update(Currency);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Currencies
        [ResponseType(typeof(Currency))]
        public IHttpActionResult PostCurrency(Currency Currency)
        {
            Currency.Id = Guid.NewGuid();
            currencyRepository.Insert(Currency);

            return CreatedAtRoute("DefaultApi", new { id = Currency.Id }, Currency);
        }

        // DELETE: api/Currencies/5
        [ResponseType(typeof(Currency))]
        public IHttpActionResult DeleteCurrency(Guid id)
        {
            Currency Currency = currencyRepository.GetById(id);
            if (Currency == null)
            {
                return NotFound();
            }
            currencyRepository.Delete(Currency);

            return Ok(Currency);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}