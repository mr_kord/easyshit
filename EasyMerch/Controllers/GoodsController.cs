﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyMerch.DAL;
using EasyMerch.Models.Dictionaries;
using EasyMerch.DAL.Repositories;
using EasyMerch.Models.General;

namespace EasyMerch.Controllers
{
    public class GoodsController : ApiController
    {
        private MerchContext db = new MerchContext();
        private IRepository<Good> goodRepository;

        public GoodsController()
        {
            this.goodRepository = new Repository<Good>(db);
        }
        public GoodsController(IRepository<Good> repository)
        {
            this.goodRepository = repository;
        }


        // GET: api/Goods
        public IQueryable<Good> GetGoods()
        {
            return goodRepository.GetAll();
        }

        // GET: api/Goods/5
        [ResponseType(typeof(Good))]
        public IHttpActionResult GetGood(Guid id)
        {
            Good Good = goodRepository.GetById(id);
            if (Good == null)
            {
                return NotFound();
            }

            return Ok(Good);
        }

        // PUT: api/Goods/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutGood(Good Good)
        {
            goodRepository.Update(Good);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Goods
        [ResponseType(typeof(Good))]
        public IHttpActionResult PostGood(Good Good)
        {
            Good.Id = Guid.NewGuid();
            goodRepository.Insert(Good);

            return CreatedAtRoute("DefaultApi", new { id = Good.Id }, Good);
        }

        // DELETE: api/Goods/5
        [ResponseType(typeof(Good))]
        public IHttpActionResult DeleteGood(Guid id)
        {
            Good Good = goodRepository.GetById(id);
            if (Good == null)
            {
                return NotFound();
            }
            goodRepository.Delete(Good);

            return Ok(Good);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}