﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyMerch.DAL;
using EasyMerch.Models.Dictionaries;
using EasyMerch.DAL.Repositories;
using EasyMerch.Models.General;

namespace EasyMerch.Controllers
{
    public class OrderGoodDetailsController : ApiController
    {
        private MerchContext db = new MerchContext();
        private IRepository<OrderGoodDetails> orderGoodDetailsRepository;

        public OrderGoodDetailsController()
        {
            this.orderGoodDetailsRepository = new Repository<OrderGoodDetails>(db);
        }
        public OrderGoodDetailsController(IRepository<OrderGoodDetails> repository)
        {
            this.orderGoodDetailsRepository = repository;
        }


        // GET: api/OrderGoodDetails
        public IQueryable<OrderGoodDetails> GetOrderGoodDetails()
        {
            return orderGoodDetailsRepository.GetAll();
        }

        // GET: api/OrderGoodDetails/5
        [ResponseType(typeof(OrderGoodDetails))]
        public IHttpActionResult GetOrderGoodDetails(Guid id)
        {
            OrderGoodDetails OrderGoodDetails = orderGoodDetailsRepository.GetById(id);
            if (OrderGoodDetails == null)
            {
                return NotFound();
            }

            return Ok(OrderGoodDetails);
        }

        // PUT: api/OrderGoodDetails/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutOrderGoodDetails(OrderGoodDetails OrderGoodDetails)
        {
            orderGoodDetailsRepository.Update(OrderGoodDetails);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/OrderGoodDetails
        [ResponseType(typeof(OrderGoodDetails))]
        public IHttpActionResult PostOrderGoodDetails(OrderGoodDetails OrderGoodDetails)
        {
            OrderGoodDetails.Id = Guid.NewGuid();
            orderGoodDetailsRepository.Insert(OrderGoodDetails);

            return CreatedAtRoute("DefaultApi", new { id = OrderGoodDetails.Id }, OrderGoodDetails);
        }

        // DELETE: api/OrderGoodDetails/5
        [ResponseType(typeof(OrderGoodDetails))]
        public IHttpActionResult DeleteOrderGoodDetails(Guid id)
        {
            OrderGoodDetails OrderGoodDetails = orderGoodDetailsRepository.GetById(id);
            if (OrderGoodDetails == null)
            {
                return NotFound();
            }
            orderGoodDetailsRepository.Delete(OrderGoodDetails);

            return Ok(OrderGoodDetails);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}