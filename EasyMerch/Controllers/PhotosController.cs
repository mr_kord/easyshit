﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyMerch.DAL;
using EasyMerch.Models.Dictionaries;
using EasyMerch.DAL.Repositories;
using EasyMerch.Models.General;

namespace EasyMerch.Controllers
{
    public class PhotosController : ApiController
    {
        private MerchContext db = new MerchContext();
        private IRepository<Photo> photoRepository;

        public PhotosController()
        {
            this.photoRepository = new Repository<Photo>(db);
        }
        public PhotosController(IRepository<Photo> repository)
        {
            this.photoRepository = repository;
        }


        // GET: api/Photos
        public IQueryable<Photo> GetPhotos()
        {
            return photoRepository.GetAll();
        }

        // GET: api/Photos/5
        [ResponseType(typeof(Photo))]
        public IHttpActionResult GetPhoto(Guid id)
        {
            Photo Photo = photoRepository.GetById(id);
            if (Photo == null)
            {
                return NotFound();
            }

            return Ok(Photo);
        }

        // PUT: api/Photos/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutPhoto(Photo Photo)
        {
            photoRepository.Update(Photo);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Photos
        [ResponseType(typeof(Photo))]
        public IHttpActionResult PostPhoto(Photo Photo)
        {
            Photo.Id = Guid.NewGuid();
            photoRepository.Insert(Photo);

            return CreatedAtRoute("DefaultApi", new { id = Photo.Id }, Photo);
        }

        // DELETE: api/Photos/5
        [ResponseType(typeof(Photo))]
        public IHttpActionResult DeletePhoto(Guid id)
        {
            Photo Photo = photoRepository.GetById(id);
            if (Photo == null)
            {
                return NotFound();
            }
            photoRepository.Delete(Photo);

            return Ok(Photo);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}