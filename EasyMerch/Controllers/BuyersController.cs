﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyMerch.DAL;
using EasyMerch.Models.Dictionaries;
using EasyMerch.DAL.Repositories;

namespace EasyMerch.Controllers
{
    public class BuyersController : ApiController
    {
        private MerchContext db = new MerchContext();
        private IRepository<Buyer> buyerRepository;

        public BuyersController()
        {
            this.buyerRepository = new Repository<Buyer>(db);
        }
        public BuyersController(IRepository<Buyer> repository)
        {
            this.buyerRepository = repository;
        }


        // GET: api/Buyers
        public IQueryable<Buyer> GetBuyers()
        {
            return buyerRepository.GetAll();
        }

        // GET: api/Buyers/5
        [ResponseType(typeof(Buyer))]
        public IHttpActionResult GetBuyer(Guid id)
        {
            Buyer Buyer = buyerRepository.GetById(id);
            if (Buyer == null)
            {
                return NotFound();
            }

            return Ok(Buyer);
        }

        // PUT: api/Buyers/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutBuyer(Buyer Buyer)
        {
            buyerRepository.Update(Buyer);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Buyers
        [ResponseType(typeof(Buyer))]
        public IHttpActionResult PostBuyer(Buyer Buyer)
        {
            Buyer.Id = Guid.NewGuid();
            buyerRepository.Insert(Buyer);

            return CreatedAtRoute("DefaultApi", new { id = Buyer.Id }, Buyer);
        }

        // DELETE: api/Buyers/5
        [ResponseType(typeof(Buyer))]
        public IHttpActionResult DeleteBuyer(Guid id)
        {
            Buyer Buyer = buyerRepository.GetById(id);
            if (Buyer == null)
            {
                return NotFound();
            }

            buyerRepository.Delete(Buyer);

            return Ok(Buyer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}