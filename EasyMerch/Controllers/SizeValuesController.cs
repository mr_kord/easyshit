﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyMerch.DAL;
using EasyMerch.Models.Dictionaries;
using EasyMerch.DAL.Repositories;
using EasyMerch.Models.General;

namespace EasyMerch.Controllers
{
    public class SizeValuesController : ApiController
    {
        private MerchContext db = new MerchContext();
        private IRepository<SizeValue> sizeValueRepository;

        public SizeValuesController()
        {
            this.sizeValueRepository = new Repository<SizeValue>(db);
        }
        public SizeValuesController(IRepository<SizeValue> repository)
        {
            this.sizeValueRepository = repository;
        }


        // GET: api/SizeValues
        public IQueryable<SizeValue> GetSizeValues()
        {
            return sizeValueRepository.GetAll();
        }

        // GET: api/SizeValues/5
        [ResponseType(typeof(SizeValue))]
        public IHttpActionResult GetSizeValue(Guid id)
        {
            SizeValue SizeValue = sizeValueRepository.GetById(id);
            if (SizeValue == null)
            {
                return NotFound();
            }

            return Ok(SizeValue);
        }

        // PUT: api/SizeValues/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutSizeValue(SizeValue SizeValue)
        {
            sizeValueRepository.Update(SizeValue);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/SizeValues
        [ResponseType(typeof(SizeValue))]
        public IHttpActionResult PostSizeValue(SizeValue SizeValue)
        {
            SizeValue.Id = Guid.NewGuid();
            sizeValueRepository.Insert(SizeValue);

            return CreatedAtRoute("DefaultApi", new { id = SizeValue.Id }, SizeValue);
        }

        // DELETE: api/SizeValues/5
        [ResponseType(typeof(SizeValue))]
        public IHttpActionResult DeleteSizeValue(Guid id)
        {
            SizeValue SizeValue = sizeValueRepository.GetById(id);
            if (SizeValue == null)
            {
                return NotFound();
            }
            sizeValueRepository.Delete(SizeValue);

            return Ok(SizeValue);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}