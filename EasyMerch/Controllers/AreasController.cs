﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using EasyMerch.DAL;
using EasyMerch.Models.Dictionaries;
using EasyMerch.DAL.Repositories;

namespace EasyMerch.Controllers
{
    public class AreasController : ApiController
    {
        private MerchContext db = new MerchContext();
        private IRepository<Area> areaRepository;

        public AreasController()
        {
            this.areaRepository = new Repository<Area>(db);
        }
        public AreasController(IRepository<Area> repository)
        {
            this.areaRepository = repository;
        }


        // GET: api/Areas
        public IQueryable<Area> GetAreas()
        {
            return areaRepository.GetAll();
        }

        // GET: api/Areas/5
        [ResponseType(typeof(Area))]
        public IHttpActionResult GetArea(Guid id)
        {
            Area area = areaRepository.GetById(id);
            if (area == null)
            {
                return NotFound();
            }

            return Ok(area);
        }

        // PUT: api/Areas/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutArea(Area area)
        {
            areaRepository.Update(area);

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Areas
        [ResponseType(typeof(Area))]
        public IHttpActionResult PostArea(Area area)
        {
            area.Id = Guid.NewGuid();
            areaRepository.Insert(area);

            return CreatedAtRoute("DefaultApi", new { id = area.Id }, area);
        }

        // DELETE: api/Areas/5
        [ResponseType(typeof(Area))]
        public IHttpActionResult DeleteArea(Guid id)
        {
            Area area = areaRepository.GetById(id);
            if (area == null)
            {
                return NotFound();
            }
            areaRepository.Delete(area);

            return Ok(area);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}