﻿using EasyMerch.Models.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyMerch.Models.Dictionaries
{
    public class Buyer
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set;}

        public Buyer()
        {
            this.Orders = new List<Order>();
        }
        public virtual ICollection<Order> Orders { get; set; }
    }
}