﻿using EasyMerch.Models.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyMerch.Models.Dictionaries
{
    public class Collection
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public Collection()
        {
            this.Orders = new List<Order>();
        }
        public virtual ICollection<Order> Orders { get; set; }
    }
}