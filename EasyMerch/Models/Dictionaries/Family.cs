﻿using EasyMerch.Models.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyMerch.Models.Dictionaries
{
    public class Family
    {
        public Family()
        {
            this.Goods = new List<Good>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Good> Goods { get; set; }
    }
}