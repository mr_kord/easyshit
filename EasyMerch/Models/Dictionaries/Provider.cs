﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyMerch.Models.Dictionaries
{
    public class Provider
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public Provider()
        {
            this.Orders = new List<Provider>();
        }
        public virtual ICollection<Provider> Orders { get; set; }
    }
}