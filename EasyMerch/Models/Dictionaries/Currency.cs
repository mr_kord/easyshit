﻿using EasyMerch.Models.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyMerch.Models.Dictionaries
{
    public class Currency
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public Currency()
        {
            this.Orders = new List<Order>();
        }
        public virtual ICollection<Order> Orders { get; set; }
    }
}