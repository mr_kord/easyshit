﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyMerch.Models.ScaleSizes
{
    public class Scale
    {
        public Scale()
        {
            this.Sizes = new List<Size>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }

        public  virtual ICollection<Size> Sizes { get; set; }
    }
}