﻿using EasyMerch.Models.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyMerch.Models.ScaleSizes
{
    public class Size
    {
        public Size()
        {
            this.SizeValues = new List<SizeValue>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }

        public virtual Scale Scale { get; set; }
       
        public virtual ICollection<SizeValue> SizeValues { get; set; }
       

    }
}