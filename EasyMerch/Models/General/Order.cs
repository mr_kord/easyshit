﻿using EasyMerch.Models.Dictionaries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyMerch.Models.General
{
    public class Order
    {
        public Order()
        {
            this.OrderGoods = new List<OrderGood>();
        }
       
        public Guid Id { get; set; }
        public virtual Brand Brand { get; set; }
        public virtual Buyer Buyer { get; set; }
        public virtual Currency Currency { get; set; }
        

        public virtual OrderType OrderType { get; set; }
        public virtual Season Season { get; set; }
        public virtual Collection Collection { get; set; }
        public virtual Area Area { get; set; }
        public virtual Provider Provider { get; set; }

        public virtual ICollection<OrderGood> OrderGoods { get; set; }


        //  public virtual Photo Photo { get; set; }
    }
}