﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyMerch.Models.General
{
    public class OrderGood
    {
        public Guid Id { get; set; }

        public Guid OrderId { get; set; }
        public Guid GoodId { get; set; }

        public OrderGood()
        {
            this.OrderGoodDetails = new List<OrderGoodDetails>();
        }

        public virtual ICollection<OrderGoodDetails> OrderGoodDetails { get; set; }

        public virtual ICollection<Order> Orders { get; set; }
        public virtual ICollection<Good> Goods { get; set; }
    }
}