﻿using EasyMerch.Models.ScaleSizes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyMerch.Models.General
{
    public class SizeValue
    {
        public Guid Id { get; set; }

       
        public int OrderAmmount { get; set; } // заказаное количество
        public int OrderConfirmationAmmoount { get; set; } // подтверденное количество товара
        public int OrderDeliveryAmmount { get; set; } // количество поставленного


        public virtual Size Size { get; set; }
        public virtual OrderGoodDetails OrderGoodDetails { get; set; }

    }
}