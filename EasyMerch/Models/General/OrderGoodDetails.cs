﻿using EasyMerch.Models.Dictionaries;
using EasyMerch.Models.ScaleSizes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyMerch.Models.General
{
    public class OrderGoodDetails
    {
        public Guid Id { get; set; }
        public virtual OrderGood OrderGood { get; set; }

        public decimal? Markup { get; set; }
        public decimal? PurchasePrice { get; set; }


        public OrderGoodDetails()
        {
            this.SizeValues = new List<SizeValue>();
        }

        public  virtual ICollection<SizeValue> SizeValues { get; set; }



      

    }
}