﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyMerch.Models.General
{
    public class Photo
    {
        public Guid Id { get; set; }

        public string Url { get; set; }

        public string Type { get; set; } // TODO:what it the type? 

        public virtual Good Good { get; set; }

        public Photo()
        {
            this.Orders = new List<Order>();
        }
        public virtual ICollection<Order> Orders { get; set; }
    }

    
}