﻿using EasyMerch.Models.Dictionaries;
using EasyMerch.Models.ScaleSizes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyMerch.Models.General
{
    public class Good
    {
        public Good()
        { 
            this.Photos = new List<Photo>();
           this.OrderGoods = new List<OrderGood>();
        }

        public Guid Id { get; set; }

        public virtual KTT KTT { get; set; }

        public bool CarryOver { get; set; }
        public bool BestSeller { get; set; }
        public bool Permament { get; set; }
        public string Article { get; set; }
        public string ColorCode { get; set; }
        public string MaterialCode { get; set; }


        public virtual CostSegment CostSegment { get; set; } 
        public virtual Family Family { get; set; }
        


        public virtual ICollection<Photo> Photos { get; set; }
        
        public virtual ICollection<OrderGood> OrderGoods { get; set; }
    }
}